import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PaiementResumeComponent } from './paiement-resume/paiement-resume.component';
import { ProductListComponent } from './product-list/product-list.component';
import { DashboardOptionsComponent } from './dashboard-options/dashboard-options.component';
import { SellPauseComponent } from './sell-pause/sell-pause.component';
import { FestivalComponent } from './festival/festival.component';
import { ClientInfoComponent } from './client-info/client-info.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PaiementResumeComponent,
    ProductListComponent,
    DashboardOptionsComponent,
    SellPauseComponent,
    FestivalComponent,
    ClientInfoComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
