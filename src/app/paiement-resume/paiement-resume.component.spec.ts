import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaiementResumeComponent } from './paiement-resume.component';

describe('PaiementResumeComponent', () => {
  let component: PaiementResumeComponent;
  let fixture: ComponentFixture<PaiementResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaiementResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaiementResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
